# Projekat ISA 2019/20 BackEnd
Backend je realizovan uz pomoc Symfony (v4.4) tehnologije. Da bi se backend projekat uspjesno pokrenuo
potrebno je imati instalirane sledece programe:
- Composer v1.6.3
- PHP 7.2.31 (cli)
- XAMPP v3.2.4

### Preuzimanje projekat
Kako biste uspjesno preuzeli projekat potrebno je preuzeti projekat komandom:
- **git clone https://gitlab.com/ognjennn77/isabackend.git**

### Pokretanje projekat
- najprije je potrebno pokrenuti XAMPP control panel i obezbijediti da su server i baza pokrenuti
- Otvoriti preuzeti fajl u nekom okruzenju (Visual Studio Code)
- Nakon otvaranja potrebno je uneti komadu za instalaciju dependancy-a: **composer install**
- Kada su dependancy-i instalirani potrebno je porenuti php server : **php bin/console init:test:environment**
- Ova komanda ce izgenerisati public i secret key koje je potrebno prepisati na frontu u config.js

### Testovi
kako bi pokrenuli testove potrebno je da u konzolu upisete sledecu komandu: **php bin/phpunit**
