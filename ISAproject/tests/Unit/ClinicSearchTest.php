<?php

namespace App\Tests\Unit;

use App\Entity\User;
use App\Entity\Clinic;
use App\Entity\Examination;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClinicSearchTest extends WebTestCase
{
    private $em;

    protected function setUp()
    {
        static::bootKernel();

        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function tearDown()
    {
        $this->em = null;
    }


    public function testExample()
    {
        $clinic = new Clinic();
        $clinic->setName("Media group");
        $clinic->setCity("Novi Sad");
        $this->em->persist($clinic);
        $this->em->flush();

        $clinics = $this->em->getRepository(Clinic::class)->findBy(['name' => $clinic->getName()]);
        $this->assertTrue($clinics != []);

        $clinics = $this->em->getRepository(Clinic::class)->findBy(['name' => $clinic->getName(), 'city' => $clinic->getCity()]);
        $this->assertTrue($clinics != []);

        $clinics = $this->em->getRepository(Clinic::class)->findBy(['city' => $clinic->getCity()]);
        $this->assertTrue($clinics != []);

        $clinics = $this->em->getRepository(Clinic::class)->findBy(['city' => "Bijeljina"]);
        $this->assertFalse($clinics != []);

        $clinics = $this->em->getRepository(Clinic::class)->findBy(['name' => "Sveti Vracevi"]);
        $this->assertFalse($clinics != []);
    }
}