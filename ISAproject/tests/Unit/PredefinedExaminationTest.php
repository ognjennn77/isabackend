<?php

namespace App\Tests\Unit;

use App\Entity\User;
use App\Entity\Clinic;
use App\Entity\Examination;
use App\Enum\ExaminationType;
use App\Enum\ExaminationStatus;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PredefinedExaminationTest extends WebTestCase
{
    private $em;

    protected function setUp()
    {
        static::bootKernel();

        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function tearDown()
    {
        $this->em = null;
    }


    public function testExample()
    {

        $patient = $this->em->getRepository(User::class)->findOneBy(['email' => "patient1@gmail.com"]);

        $examination = new Examination();
        $examination->setType(ExaminationType::PREDEFINED);
        $examination->setStatus(ExaminationStatus::PENDING);
        $examination->setPatient($patient);

        $this->assertEquals($patient->getId(), $examination->getPatient()->getId());
    }
}