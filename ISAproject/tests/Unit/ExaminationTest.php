<?php

namespace App\Tests\Unit;

use App\Entity\User;
use App\Entity\Examination;
use PHPUnit\Framework\TestCase;

class ExaminationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $examination = new Examination();
        $patient = new User();
        $patient->setEmail("pero@gmail.com");
        $examination->setPatient($patient);

        $this->assertEquals($examination->getPatient()->getEmail(), $patient->getEmail());
    }
}
