<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PredefinedControllerTest extends WebTestCase
{

    public function testPredefinedExamination(){
        $client = static::createClient();

        $json = json_encode([
            "examinationId" => 2,
            "version" => 2,
            "test" => true
        ]);


        $client->request(
            'PUT', 
            'api/change_examination_status', 
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $json
        );


        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}