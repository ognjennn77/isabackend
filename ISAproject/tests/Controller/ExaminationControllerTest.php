<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExaminationControllerTest extends WebTestCase
{

    public function testGetExamination()
    {
        $client = static::createClient();

        $json = json_encode([
            "type" => 2,
            "date" => "2021-03-03T02:00",
            "clinic" => null,
            "city" => null,
            "doctor" => null
        ]);

        $client->request(
            'POST', 
            '/api/get_examination', 
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $json
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateExamination(){
        $client = static::createClient();

        $json = json_encode([
            "doctorId" => 8,
            "date" => "2021-03-03T03:00",
            "test" => true
        ]);


        $client->request(
            'POST', 
            '/api/create_examination', 
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $json
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

}