<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClinicControllerTest extends WebTestCase
{

    public function testSearchClinic()
    {
        $client = static::createClient();

        $client->request('GET', '/api/get_clinics?city=Novi+Sad');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}