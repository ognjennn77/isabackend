<?php


namespace App\Enum;


abstract class DoctorType
{
    const STOMATOLOG = 0;
    const PEDIJATAR = 1;
    const OPSTA_PRAKSA = 2;
    const KARDIOLOG = 3;
    const FIZIJATAR = 4;
    const UROLOG = 5;
    const GINEKOLOG = 6;
}