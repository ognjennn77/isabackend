<?php


namespace App\Enum;


abstract class UserType
{
    const DOCTOR = 0;
    const NURSE = 1;
    const PATIENT = 2;
}