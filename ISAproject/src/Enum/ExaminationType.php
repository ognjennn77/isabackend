<?php


namespace App\Enum;


abstract class ExaminationType
{
    const PREDEFINED = 0;
    const ORDINARY = 1;
}