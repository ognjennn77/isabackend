<?php


namespace App\Enum;


abstract class ExaminationStatus
{
    const PENDING = 0;
    const ACCEPTED = 1;
}