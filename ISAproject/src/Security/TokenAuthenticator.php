<?php

namespace App\Security;

use App\Entity\AccessToken;
use App\Entity\SocialAccessToken;
use Doctrine\ORM\EntityManagerInterface;
use OAuth2\OAuth2;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;


class TokenAuthenticator extends AbstractGuardAuthenticator
{
    protected $serverService;
    private $em;

    public function __construct(OAuth2 $serverService, EntityManagerInterface $em){
        $this->serverService = $serverService;
        $this->em = $em;
    }

    public function supports(Request $request)
    {
        return $request->headers->has('Authorization');
    }

    public function getCredentials(Request $request)
    {
        return array(
            'token' => $this->serverService->getBearerToken($request, true),
            'socialToken' => $this->getSocialToken($request)
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = $credentials['token'];
        $socialToken = $credentials['socialToken'];

        if (null === $token && $socialToken === null) {
            return;
        }

        return $token === null ? $this->getUserBySocialToken($socialToken) : $this->getUserByToken($token);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

        );

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }

    public function getSocialToken($request)
    {
        $headerToken = $request->headers->get('Authorization');

        $data = explode(' ', $headerToken);

        if(count($data) != 2 || $data[0] != 'Social')
        {
            return null;
        }

        return $data[1];
    }

    public function getUserByToken($token) {

        $accessToken = $this->em->getRepository(AccessToken::class)->findOneBy(['token' => $token]);

        if(is_null($accessToken) || $accessToken->hasExpired())
        {
            return null;
        }

        return $accessToken->getUser($token);
    }

    public function getUserBySocialToken($token)
    {
        $socialAccessToken = $this->em->getRepository(SocialAccessToken::class)->findOneBy(['token' => $token]);

        if(is_null($socialAccessToken) /*|| $socialAccessToken->hasExpired()*/)
        {
            return null;
        }

        return $socialAccessToken->getUser();
    }
}