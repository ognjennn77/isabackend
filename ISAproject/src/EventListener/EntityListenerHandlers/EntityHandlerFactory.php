<?php

namespace App\EventListener\EntityListenerHandlers;


use App\Entity\AccessToken;
use App\Entity\Client;
use App\Entity\PostTag;
use App\Entity\RefreshToken;
use App\Entity\SocialAccessToken;

class EntityHandlerFactory
{
    public static function getHandler($entity, $container)
    {
        $listener = null;

        if( $entity instanceof Client ||
            $entity instanceof SocialAccessToken ||
            $entity instanceof AccessToken ||
            $entity instanceof RefreshToken)
        {
            return $listener;
        }
        else {
            $listener = new BaseListenerHandler();
            $listener->setEntity($entity);
        }

        return $listener;
    }
}