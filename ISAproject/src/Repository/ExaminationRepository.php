<?php

namespace App\Repository;

use App\Entity\Examination;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class ExaminationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Examination::class);
    }

    public function getExaminations($date) {

        $qb = $this->createQueryBuilder('examination')
        ->select('examination')
        ->where('examination.start_date <= :date')
        ->andWhere('examination.end_date >= :date')
        ->setParameter('date', $date);

        return $qb->getQuery()->getResult();
    }
}