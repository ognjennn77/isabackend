<?php

namespace App\Entity;

class Hall
{
    protected $id;

    private $number;

    private $clinic;

    protected $date_created;

    protected $deleted;

    protected $date_updated;

    /**
     * Get the value of number
     */ 
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set the value of number
     *
     */ 
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * Get the value of clinic
     */ 
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * Set the value of clinic
     *
     */ 
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of date_created
     */ 
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set the value of date_created
     *
     * @return  self
     */ 
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Get the value of date_updated
     */ 
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set the value of date_updated
     *
     * @return  self
     */ 
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Get the value of deleted
     */ 
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set the value of deleted
     *
     * @return  self
     */ 
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }
}