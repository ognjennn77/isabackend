<?php

namespace App\Entity;

class Termin
{
    protected $id;

    private $examination;

    private $hall;

    private $status;

    protected $date_created;

    protected $deleted;

    protected $date_updated;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of examination
     */ 
    public function getExamination()
    {
        return $this->examination;
    }

    /**
     * Set the value of examination
     *
     */ 
    public function setExamination($examination)
    {
        $this->examination = $examination;
    }

    /**
     * Get the value of hall
     */ 
    public function getHall()
    {
        return $this->hall;
    }

    /**
     * Set the value of hall
     *
     */ 
    public function setHall($hall)
    {
        $this->hall = $hall;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     */ 
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get the value of date_created
     */ 
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set the value of date_created
     *
     * @return  self
     */ 
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Get the value of deleted
     */ 
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set the value of deleted
     *
     * @return  self
     */ 
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get the value of date_updated
     */ 
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set the value of date_updated
     *
     * @return  self
     */ 
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;

        return $this;
    }
}