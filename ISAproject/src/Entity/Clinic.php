<?php

namespace App\Entity;

class Clinic
{
    protected $id;
    
    private $name;

    private $head;

    private $cliniccenter;

    private $city;

    protected $date_created;

    protected $deleted;

    protected $date_updated;


    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     */ 
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the value of head
     */ 
    public function getHead()
    {
        return $this->head;
    }

    /**
     * Set the value of head
     *
     */ 
    public function setHead($head)
    {
        $this->head = $head;
    }

    /**
     * Get the value of cliniccenter
     */ 
    public function getCliniccenter()
    {
        return $this->cliniccenter;
    }

    /**
     * Set the value of cliniccenter
     *
     */ 
    public function setCliniccenter($cliniccenter)
    {
        $this->cliniccenter = $cliniccenter;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of date_created
     */ 
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set the value of date_created
     *
     */ 
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * Get the value of deleted
     */ 
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set the value of deleted
     *
     */ 
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Get the value of date_updated
     */ 
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set the value of date_updated
     *
     */ 
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;

    }

    /**
     * Get the value of city
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     */ 
    public function setCity($city)
    {
        $this->city = $city;
    }
}