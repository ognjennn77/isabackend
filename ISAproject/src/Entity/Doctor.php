<?php

namespace App\Entity;

class Doctor
{
    protected $id;

    private $type;

    private $clinic;

    private $doctorType;

    protected $date_created;

    protected $deleted;

    protected $date_updated;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of type
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     */ 
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get the value of clinic
     */ 
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * Set the value of clinic
     *
     */ 
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
    }

       /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $date_updated
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Get the value of doctorType
     */ 
    public function getDoctorType()
    {
        return $this->doctorType;
    }

    /**
     * Set the value of doctorType
     *
     * @return  self
     */ 
    public function setDoctorType($doctorType)
    {
        $this->doctorType = $doctorType;

        return $this;
    }
}