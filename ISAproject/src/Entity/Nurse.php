<?php

namespace App\Entity;

class Nurse
{
    protected $id;
    
    protected $date_created;

    protected $deleted;

    protected $date_updated;


    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get the value of data_created
     */ 
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set the value of data_created
     *
     */ 
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * Get the value of deleted
     */ 
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set the value of deleted
     *
     */ 
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Get the value of date_updated
     */ 
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set the value of date_updated
     *
     */ 
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }
}