<?php

namespace App\Controller;


use App\API\HandlerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UserController extends BaseController
{
    public function getCurrentUserAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $handler = $this->getHandler($request, HandlerType::User);
        $user = $handler->getCurrentUser();

        return $user;
    }

    public function register(Request $request)
    {
        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->register($request);
    }

    public function getDoctors(Request $request){
        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->getDoctors($request);
    }

    public function getClinics(Request $request){
        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->getClinics($request);
    }

    public function getExaminations(Request $request){
        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->getExaminations($request);
    }

    public function createExamination(Request $request){
        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->createExamination($request);
    }

    public function getUserExaminations(Request $request){
        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->getUserExaminations($request);
    }

    public function confirmUser(Request $request) {

        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->confirmUser();
    }

    public function confirmExamination(Request $request) {

        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->confirmExamination();
    }

    public function getPredefinedExamination(Request $request) {

        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->getPredefinedExamination();
    }

    public function changeExaminationStatus(Request $request) {

        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->changeExaminationStatus();
    }

    public function getUserProfile(Request $request) {

        $handler = $this->getHandler($request, HandlerType::User);

        return $handler->getUserProfile();
    }

}