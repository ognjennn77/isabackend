<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19.2.18.
 * Time: 20.40
 */

namespace App\Controller;

use App\Entity\AccessToken;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends  FOSRestController
{
    protected function getSerializer()
    {
        return $this->get('jms_serializer');
    }

    protected function getHandler(Request $request, $type, $admin = false)
    {
        $handler = $this->get('app.api.factory')->getHandler($request, $type, $admin);
        $handler->setRequest($request);
        return $handler;
    }

}