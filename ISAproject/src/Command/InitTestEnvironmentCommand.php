<?php


namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class InitTestEnvironmentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('init:test:environment');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        if($this->getContainer()->getParameter('kernel.environment') == 'prod') {
            //$output->writeln('Can not run command in production');
            //return;
        }

        $fs = new Filesystem();

        // Clear file system

        if($fs->exists($this->getPublicDir().'/uploads')) {

            $this->deleteDir($this->getPublicDir().'/uploads');
            $fs->mkdir($this->getPublicDir().'/uploads/images/ads');
        }

        // drop database

        $command = $this->getApplication()->find('doctrine:database:drop');
        $arguments = array(
            '--force' => true
        );

        $command->run(new ArrayInput($arguments), $output);

        // create database

        $command = $this->getApplication()->find('doctrine:database:create');
        $command->run($input, $output);

        // create scheme for database

        $command = $this->getApplication()->find('do:sc:up');
        $arguments = array(
            '--force' => true
        );
        $command->run(new ArrayInput($arguments), $output);

        //create cliniccenter
        $command = $this->getApplication()->find('cliniccenter:import:data');
        $command->run($input, $output);

        //create clinic
        $command = $this->getApplication()->find('clinic:import:data');
        $command->run($input, $output);

        //create patient
        $command = $this->getApplication()->find('patient:import:data');
        $command->run($input, $output);

        //create doctor
        $command = $this->getApplication()->find('doctor:import:data');
        $command->run($input, $output);
        
        //create nurse
        $command = $this->getApplication()->find('nurse:import:data');
        $command->run($input, $output);

        //create hall
        $command = $this->getApplication()->find('hall:import:data');
        $command->run($input, $output);

        //create medicine
        $command = $this->getApplication()->find('medicine:import:data');
        $command->run($input, $output);

        //create examination 
        $command = $this->getApplication()->find('examination:import:data');
        $command->run($input, $output);


        // crate client

        $command = $this->getApplication()->find('app:oauth-server:client-create');
        $arguments = array(
            '--redirect-uri' => [ 'http://www.google.com/' ],
            '--grant-type' => [ 'password', 'refresh_token', 'client_credentials' ],

        );
        $command->run(new ArrayInput($arguments), $output);
    }

    public function getPublicDir()
    {
        return $this->getContainer()->getParameter('kernel.project_dir') . '/public';
    }

    public function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new \InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
}