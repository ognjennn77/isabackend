<?php


namespace App\Command;

use DateTime;
use App\Entity\User;
use App\Entity\ClinicCenter;
use App\Entity\Clinic;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportClinicCommand extends ContainerAwareCommand
{
    private $em;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function configure()
    {
        $this->setName('clinic:import:data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $data = $serializer->decode(file_get_contents(__DIR__.'/../../importData/Clinic/clinic.csv'), 'csv');

        foreach ($data as $item) {

            
            $c = new Clinic();
            $c->setName($item['name']);
            $user = $this->em->getRepository(User::class)->find($item['head']);
            $c->setHead($user);
            $cc = $this->em->getRepository(ClinicCenter::class)->find($item['cliniccenter']);
            $c->setCliniccenter($cc);
            $c->setCity($item['city']);
            $cc->setDeleted(false);
            $this->em->persist($c);
            
        } 

        $this->em->flush();
    
    }

    public function getPublicDir()
    {
        return $this->getContainer()->getParameter('kernel.project_dir') . '/public';
    }
}