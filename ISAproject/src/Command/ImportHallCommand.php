<?php


namespace App\Command;

use DateTime;
use App\Entity\Hall;
use App\Entity\Clinic;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportHallCommand extends ContainerAwareCommand
{
    private $em;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function configure()
    {
        $this->setName('hall:import:data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $data = $serializer->decode(file_get_contents(__DIR__.'/../../importData/Hall/hall.csv'), 'csv');

        foreach ($data as $item) {

            
            $hall = new Hall();
            $hall->setNumber($item['number']);
            $c = $this->em->getRepository(Clinic::class)->find($item['clinic']);
            $hall->setClinic($c);
            $hall->setDeleted(false);
            $this->em->persist($hall);
            
        } 

        $this->em->flush();
    
    }

    public function getPublicDir()
    {
        return $this->getContainer()->getParameter('kernel.project_dir') . '/public';
    }
}