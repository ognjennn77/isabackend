<?php


namespace App\Command;

use DateTime;
use App\Entity\User;
use App\Enum\UserType;
use App\Entity\Doctor;
use App\Entity\Clinic;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportDoctorCommand extends ContainerAwareCommand
{
    private $em;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function configure()
    {
        $this->setName('doctor:import:data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $data = $serializer->decode(file_get_contents(__DIR__.'/../../importData/User/doktori.csv'), 'csv');

        foreach ($data as $item) {

            $user = new User();
            $user->setFirstName($item['firstName']);
            $user->setLastName($item['lastName']);
            $user->setDeleted(false);
            $user->setUserType(UserType::DOCTOR);
            $user->setJmbg($item['jmbg']);
            $user->setAddress($item['address']);
            $user->setBirthDate(new \DateTime($item['birth_date']));
            $user->setCity($item['city']);
            $user->setCountry($item['country']);
            $user->setPhone($item['phone']);
            $user->setInsurance($item['insurance']);
            $user->setEmail($item['email']);
            $user->setUsername($item['email']);
            $user->setPlainPassword("1234");
            $user->setEnabled(true);
            $user->setDateCreated(new \DateTime());
            $doctor = new Doctor();
            $doctor->setDoctorType($item['doctorType']);
            $c = $this->em->getRepository(Clinic::class)->find($item['clinic']);
            $doctor->setClinic($c);
            //$doctor->setDeleted(false);
            $this->em->persist($doctor);
            $user->setDoctor($doctor);
            $user->setDeleted(false);
            $this->em->persist($user);
            
        } 

        $this->em->flush();
    
    }

    public function getPublicDir()
    {
        return $this->getContainer()->getParameter('kernel.project_dir') . '/public';
    }
}