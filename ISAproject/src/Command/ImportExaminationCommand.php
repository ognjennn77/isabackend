<?php


namespace App\Command;

use DateTime;
use App\Entity\Hall;
use App\Entity\User;
use App\Entity\Clinic;
use App\Entity\Examination;
use App\Enum\ExaminationType;
use App\Enum\ExaminationStatus;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportExaminationCommand extends ContainerAwareCommand
{
    private $em;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function configure()
    {
        $this->setName('examination:import:data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $data = $serializer->decode(file_get_contents(__DIR__.'/../../importData/Examination/examination.csv'), 'csv');

        foreach ($data as $item) {

            //$patient = $this->em->getRepository(User::class)->find($item['patient']);
            $doctor = $this->em->getRepository(User::class)->find($item['doctor']);
            $ex = new  Examination();
            $start_date = new \DateTime();
            $start_date->modify("+" . $item['start_date'] ." day");
            $end_date = new \DateTime();
            $end_date->modify('+30 minutes');
            $end_date->modify('+' . $item['start_date'] ." days");

            //$ex->setPatient($patient);
            $ex->setDoctor($doctor);
            $ex->setStartDate($start_date);
            $ex->setEndDate($end_date);
            $ex->setType(ExaminationType::PREDEFINED);
            $ex->setStatus(ExaminationStatus::PENDING);

            $this->em->persist($ex);
        } 

        $this->em->flush();
    
    }

    public function getPublicDir()
    {
        return $this->getContainer()->getParameter('kernel.project_dir') . '/public';
    }
}