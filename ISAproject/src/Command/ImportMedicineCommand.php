<?php


namespace App\Command;

use DateTime;
use App\Entity\Medicine;
use App\Entity\User;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportMedicineCommand extends ContainerAwareCommand
{
    private $em;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function configure()
    {
        $this->setName('medicine:import:data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $data = $serializer->decode(file_get_contents(__DIR__.'/../../importData/Medicie/lijekovi.csv'), 'csv');

        foreach ($data as $item) {

            
            $med = new Medicine();
            $med->setName($item['name']);
            $nurse = $this->em->getRepository(User::class)->find($item['nurse']);
            $med->setNurse($nurse);
            $med->setDescription($item['description']);
            $med->setDeleted(false);
            $this->em->persist($med);
            
        } 

        $this->em->flush();
    
    }

    public function getPublicDir()
    {
        return $this->getContainer()->getParameter('kernel.project_dir') . '/public';
    }
}