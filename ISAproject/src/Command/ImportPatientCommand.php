<?php


namespace App\Command;

use DateTime;
use App\Entity\User;
use App\Enum\UserType;
use App\Entity\Patient;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportPatientCommand extends ContainerAwareCommand
{
    private $em;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function configure()
    {
        $this->setName('patient:import:data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $data = $serializer->decode(file_get_contents(__DIR__.'/../../importData/User/pacijenti.csv'), 'csv');

        foreach ($data as $item) {

            $user = new User();
            $user->setFirstName($item['firstName']);
            $user->setLastName($item['lastName']);
            $user->setDeleted(false);
            $user->setUserType(UserType::PATIENT);
            $user->setJmbg($item['jmbg']);
            $user->setAddress($item['address']);
            $user->setBirthDate(new \DateTime($item['birth_date']));
            $user->setCity($item['city']);
            $user->setCountry($item['country']);
            $user->setPhone($item['phone']);
            $user->setInsurance($item['insurance']);
            $user->setEmail($item['email']);
            $user->setUsername($item['email']);
            $user->setPlainPassword("1234");
            $user->setEnabled(true);
            $user->setDateCreated(new \DateTime());
            $patient = new Patient();
            $patient->setDeleted(false);
            $this->em->persist($patient);
            $user->setPatient($patient);
            $this->em->persist($user);
            
        } 

        $this->em->flush();
    
    }

    public function getPublicDir()
    {
        return $this->getContainer()->getParameter('kernel.project_dir') . '/public';
    }
}