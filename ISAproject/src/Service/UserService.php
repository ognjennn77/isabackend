<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserService
{
    private $mailService;
    private $tokenGenerator;
    private $em;
    private $container;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container,
                                MailService $mailService, TokenGeneratorService $tokenGenerator)
    {
        $this->mailService = $mailService;
        $this->tokenGenerator = $tokenGenerator;
        $this->em = $em;
        $this->container = $container;
    }

    public function sendConfirmMail(User $user) {

        $token = $this->tokenGenerator->generate();
              
        $user->setConfirmationToken($token);
        $this->em->flush();

        $this->mailService->sendMail(
            $user->getEmail(),
            'Confirm user',
            'mails/user_confirm.html.twig',
            $this->container->getParameter('app_email'),
            true,
            ['user' => $user]
        );
    }

    public function sendConfirmExaminationMail(User $user, $id) {

        $token = $this->tokenGenerator->generate();

        $this->mailService->sendMail(
            $user->getEmail(),
            'Potvrda pregleda',
            'mails/confirm_examination.html.twig',
            $this->container->getParameter('app_email'),
            true,
            ['user' => $user,
             'id'=> $id]
        );
    }

}