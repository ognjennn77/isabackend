<?php

namespace App\Service;

use FOS\UserBundle\Model\UserInterface;
use Psr\Log\LoggerInterface;
use Swift_FileSpool;
use Swift_Mailer;
use Swift_SpoolTransport;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailService
{
    protected $container;
    protected $smtpTransport;
    protected $spoolTransport;
    protected $templating;
    protected $logger;

    public function __construct(ContainerInterface $container, LoggerInterface $logger)
    {

        $this->container = $container;
        $this->logger = $logger;

        $this->templating = $this->container->get('templating');
    }

    public function sendMail($to, $subject, $template, $from, $force = false, $data = array()) {

        $mailer = null;

        if ($force) {

            $mailer = new Swift_Mailer($this->getSmtpTransport());

        } else {

            $mailer = new Swift_Mailer($this->getSpoolTransport());

        }

        $data['domain'] = $this->container->getParameter('domain');

        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $this->templating->render($template, $data),
                'text/html'
            );

        return $mailer->send($message);
    }

    public function sendBatchMail(array $to, $subject, $template, $from, $force = false, $data = array()) {

        $mailer = null;

        if ($force) {

            $mailer = new Swift_Mailer($this->getSmtpTransport());

        } else {

            $mailer = new Swift_Mailer($this->getSpoolTransport());

        }

        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setBody(
                $this->templating->render($template, $data),
                'text/html'
            );

        $failedRecipients = [];
        $numSent = 0;

        foreach ($to as $address)
        {
            $message->setTo($address);

            $numSent += $mailer->send($message, $failedRecipients);
        }

        $this->logger->info("Sent ".$numSent." messages.");

        return;
    }

    public function getSmtpTransport() : \Swift_SmtpTransport {

        /* return (new \Swift_SmtpTransport(
            getenv('MAILER_HOST'),
            getenv('MAILER_PORT'),
            getenv('MAILER_SECURITY')))
            ->setAuthMode(getenv('MAILER_AUTH_MODE'))
            ->setUsername(getenv('MAILER_USERNAME'))
            ->setPassword(getenv('MAILER_PASSWORD')); */
        return (new \Swift_SmtpTransport(
            $this->container->getParameter('mailer_host'),
            $this->container->getParameter('mailer_port'),
            $this->container->getParameter('mailer_security')))
            ->setAuthMode($this->container->getParameter('mailer_auth_mode'))
            ->setUsername($this->container->getParameter('mailer_user'))
            //->setPassword($this->container->getParameter('mailer_password'));
            ->setPassword("Aspirin!1");
    }

    public function getSpoolTransport() : Swift_SpoolTransport {

        $spool = null;

        try {

            $spool = new Swift_FileSpool(
                $this->container->getParameter('kernel.project_dir')
                .$this->container->getParameter('spool_dir'));
                //.getenv('SPOOL_DIR'));

        } catch (\Swift_IoException $e) {

            $this->logger->error($e->getMessage());

        }

        return new Swift_SpoolTransport($spool);
    }

    public function sendResettingEmailMessage(UserInterface $user) {

        $mailer = new Swift_Mailer($this->getSmtpTransport());

        $template = 'mails/password_resetting.html.twig';

        $data = array(
            'user' => $user
        );

        $message = (new \Swift_Message('Reset password'))
            ->setFrom(getenv('MAILER_USERNAME'))
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render($template, $data),
                'text/html'
            );

        return $mailer->send($message);
    }

}