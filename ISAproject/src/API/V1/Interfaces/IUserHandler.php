<?php

namespace App\API\V1\Interfaces;

interface IUserHandler
{
    public function getCurrentUser();
}