<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20.2.18.
 * Time: 17.29
 */

namespace App\API\V1;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseHandler
{
    protected $em;
    protected $container;
    protected $serializer;
    protected $user;
    protected $logger;
    protected $request;
    protected $params;
    protected $elasticaManager;

    function __construct(EntityManagerInterface $em, ContainerInterface $container, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->container = $container;
        $this->logger = $logger;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
        $this->params = $this->getParams($request);
    }

    public function getParams(Request $request)
    {
        $params = array();
        $content = $request->getContent();
        if(!empty($content))
        {
            $params = json_decode($content);
        }

        return $params;
    }

    protected function getResponse($result = [], $statusCode = Response::HTTP_OK)
    {
        $response = $this->createResponse($statusCode);

        $json = $this->getSerializer()->serialize($result, 'json');
        $response->setData(json_decode($json, true));

        return $response;
    }

    protected function createResponse($statusCode = Response::HTTP_OK)
    {
        $response = new JsonResponse();
        $response->setStatusCode($statusCode);
        $response->headers->set('Access-Control-Allow-Origin', $this->container->getParameter('allowed_origins'));
        return $response;
    }

    public function getUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

    public function getParameter($paramName) {

        return $this->request->get($paramName);
    }

    public function getSerializer() {

        if(!$this->serializer) {
            $this->serializer = $this->container->get('jms_serializer');
        }

        return $this->serializer;
    }

    public function getElasticaManager() {

        if(!$this->elasticaManager) {
            $this->elasticaManager = $this->container->get('fos_elastica.manager');
        }

        return $this->elasticaManager;
    }

    /* PAGINATION */

    public function getPage() {

        $page = $this->getParameter('page');

        if(isset($page)) {
            return $page;
        }

        if(isset($this->params->page)) {
            return $this->params->page;
        }

        return 1;
    }

    public function getPerPage() {

        $perPage = $this->getParameter('perPage');

        if(isset($perPage)) {
            return $perPage;
        }

        if(isset($this->params->perPage)) {
            return $this->params->perPage;
        }

        return $this->container->getParameter('perPage');
    }

    /* RESPONSES  */

    public function getSuccessResponse() {
        return $this->getResponse([
            'message' => 'success'
        ], Response::HTTP_OK);
    }

    public function getErrorResponse($message)
    {
        return $this->getResponse([
            'message' => $message
        ], Response::HTTP_BAD_REQUEST);
    }

    public function getParameterMissingResponse()
    {
        return $this->getResponse([
            'message' => 'parametersMissing'
        ], Response::HTTP_BAD_REQUEST);
    }

    public function getNotFoundResponse() {
        return $this->getResponse([], Response::HTTP_NOT_FOUND);
    }

    public function getNoContentResponse() {
        return $this->getResponse([], Response::HTTP_NO_CONTENT);
    }

    public function getCreatedResponse() {
        return $this->getResponse([], Response::HTTP_CREATED);
    }

    // FUNCTIONALITIES

    public function delete() {
        
        $id = $this->getParameter($this->idSelector);

        if(!isset($id)) {
            return $this->getParameterMissingResponse();
        }

        $entity = $this->em->getRepository($this->class)->find($id);

        if(!$entity) {
            return $this->getNotFoundResponse();
        }

        $entity->setDeleted(true);

        $this->em->flush();

        return $this->getNoContentResponse();
    }

    public function get() {
        $id = $this->getParameter($this->idSelector);

        if(!isset($id)) {
            return $this->getParameterMissingResponse();
        }

        $entity = $this->em->getRepository($this->class)->findOneBy(['id' => $id, 'deleted' => false]);

        if(!$entity) {
            return $this->getNotFoundResponse();
        }

        return $this->getResponse([
            'entity' => $entity
        ]);
    }
}