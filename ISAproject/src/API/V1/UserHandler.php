<?php

namespace App\API\V1;

use App\Entity\User;
use App\Entity\Clinic;
use App\Enum\UserType;
use App\Entity\Patient;
use App\Entity\Examination;
use Doctrine\DBAL\LockMode;
use Psr\Log\LoggerInterface;
use App\Enum\ExaminationType;
use App\Enum\ExaminationStatus;
use Psr\Container\ContainerInterface;
use Symfony\Component\Mercure\Update;
use App\API\V1\Interfaces\IUserHandler;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\UserCustomerPreferenceModel;
use Symfony\Component\HttpFoundation\Response;

class UserHandler extends BaseHandler implements IUserHandler
{
    function __construct(EntityManagerInterface $em, ContainerInterface $container, LoggerInterface $logger)
    {
        parent::__construct($em, $container, $logger);

        $this->userService = $container->get('app.user.service');
    }

    public function getCurrentUser()
    {
        return $this->getResponse([
            'user' => $this->getUser()
        ]);
    }

    private function createNewUser() {
        $user = new User();
        $user->setEmail($this->params->email);
        $user->setEnabled(true);
        $user->setVerified(false);
        $user->setPlainPassword($this->params->password);

        return $user;
    }

    public function register() {
        $user = new User();
        $user->setFirstName($this->params->firstName);
        $user->setLastName($this->params->lastName);
        $user->setDeleted(false);
        $user->setUserType(UserType::PATIENT);
        $user->setJmbg($this->params->jmbg);
        $user->setAddress($this->params->address);
        $user->setBirthDate(date_create_from_format('Y-m-d', $this->params->dateOfBirth));
        $user->setCity($this->params->city);
        $user->setCountry($this->params->country);
        $user->setPhone($this->params->phone);
        $user->setInsurance($this->params->insurance);
        $user->setEmail($this->params->email);
        $user->setUsername($this->params->email);
        $user->setPlainPassword($this->params->password);
        $user->setEnabled(false);
        $user->setDateCreated(new \DateTime());
        $patient = new Patient();
        $patient->setDeleted(false);
        $clinic = $this->em->getRepository(Clinic::class)->find($this->params->clinic->id);
        $patient->setClinic($clinic);
        $this->em->persist($patient);
        $user->setPatient($patient);
        $this->em->persist($user);

        $this->em->flush();

        $this->userService->sendConfirmMail($user);

        return $this->getCreatedResponse();
    }

    public function confirmUser() {

        if(!isset($this->params->token)) {
            return $this->getParameterMissingResponse();
        }

        $user = $this->em->getRepository(User::class)->findOneBy([ 'confirmationToken' => $this->params->token ]);

        if(!$user) {
            return $this->getNotFoundResponse();
        }

        $user->setEnabled(true);
        $user->setConfirmationToken(null);

        $this->em->flush();

        return $this->getResponse();
    }

    public function getDoctors(){

        $clinic = $this->request->get('clinic');
        $type = $this->request->get('type');
        $city = $this->request->get('city');

        if(!isset($clinic) && !isset($type)){
            $doctors = $this->em->getRepository(User::class)->findBy(['user_type' => UserType::DOCTOR]);
        }elseif(!isset($clinic) && isset($type)){
            $doctors = $this->em->getRepository(User::class)->findBy(['user_type' => UserType::DOCTOR]);
            $newDoctors = [];
            foreach($doctors as $doctor){
                if($doctor->getDoctor()->getDoctorType() == $type){
                    array_push($newDoctors, $doctor);
                }
            }
            $doctors = $newDoctors;
        }elseif(isset($clinic) && !isset($type)){
            $doctors = $this->em->getRepository(User::class)->findBy(['user_type' => UserType::DOCTOR]);
            $newDoctors = [];
            foreach($doctors as $doctor){
                if($doctor->getDoctor()->getClinic()->getId() == $clinic){
                    array_push($newDoctors, $doctor);
                }
            }
            $doctors = $newDoctors;
        }else{
            $doctors = $this->em->getRepository(User::class)->findBy(['user_type' => UserType::DOCTOR]);
            $newDoctors = [];
            foreach($doctors as $doctor){
                if($doctor->getDoctor()->getClinic()->getId() == $clinic && $doctor->getDoctor()->getDoctorType() == $type){
                    array_push($newDoctors, $doctor);
                }
            }
            $doctors = $newDoctors;
        }

        if(isset($city)){
            $newDoctors = [];
            foreach($doctors as $doctor){
                if($doctor->getCity() == $city){
                    array_push($newDoctors, $doctor);
                }
            }
            $doctors = $newDoctors;
        } 

        return $this->getResponse([
            'doctors' => $doctors
        ]);
    }

    public function getClinics(){

        $city = $this->request->get('city');

        $clinics = $this->em->getRepository(Clinic::class)->findAll();

        $newClinics = [];

        if(isset($city)){
            foreach($clinics as $clinic){
                if($clinic->getCity() == $city){
                    array_push($newClinics, $clinic);
                }
            }
            $clinics = $newClinics;
        }

        return $this->getResponse([
            'clinics' => $clinics
        ]);
    }

    public function getExaminations(){
        $clinic = $this->params->clinic;
        $type = $this->params->type; 
        $city = $this->params->city; 
        $doctorId = $this->params->doctor; 

        $date =  new \DateTime($this->params->date);
        $examinations = $this->em->getRepository(Examination::class)->getExaminations($date);
        $doctors = $this->em->getRepository(User::class)->findBy(['user_type' => UserType::DOCTOR]);

        $availableDoctors = [];
        foreach($doctors as $doctor){
            if($this->isAvailable($examinations, $doctor)){
                $flag = true;

                if(isset($clinic)){
                    if($doctor->getDoctor()->getClinic()->getId() != $clinic){
                        $flag = false;
                    }
                }

                if(isset($type)){
                    if($doctor->getDoctor()->getDoctorType() != $type){
                        $flag = false;
                    }
                }

                if(isset($city)){
                    if($doctor->getCity() != $city){
                        $flag = false;
                    }
                }

                if(isset($doctorId)){
                    if($doctor->getId() != $doctorId){
                        $flag = false;
                    }
                }

                if($flag){
                    array_push($availableDoctors, $doctor);
                }

            }
        }

        return $this->getResponse([
            'examinations' => $availableDoctors
        ]);

    }

    public function createExamination(){

        $this->em->getConnection()->beginTransaction();
        try {
            
            if(isset($this->params->test)){
                $user = $this->em->getRepository(User::class)->find(1);
            }else{
                $user = $this->getUser();
            }
            
            $doctor = $this->em->getRepository(User::class)->find($this->params->doctorId);
            $date = new \DateTime($this->params->date);
            $endDate = new \DateTime($this->params->date);
            $endDate->modify("+30 minutes");
            
            $examination = new Examination();
            $examination->setStartDate($date);
            $examination->setEndDate($endDate);
            $examination->setPatient($user);
            $examination->setDoctor($doctor);
            $examination->setType(ExaminationType::ORDINARY);
            $examination->setStatus(ExaminationStatus::PENDING);

            $this->em->persist($examination);
            $this->em->flush();

            $this->em->getConnection()->commit();

            if(!is_null($user)){
                $this->userService->sendConfirmExaminationMail($user, $examination->getId());
            }

            return $this->getCreatedResponse();

        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            return $this->getErrorResponse();
        }
    }


    public function getUserExaminations(){
        $sort = $this->request->get('sort');

        $user = $this->getUser();
        $examinations = $this->em->getRepository(Examination::class)->findBy(['patient' => $user, 'type' => ExaminationType::ORDINARY], array($sort => 'DESC'));
   
        return $this->getResponse([
            'examinations' =>  $examinations
        ]);
    }

    
    public function confirmExamination() {

        if(!isset($this->params->id)) {
            return $this->getParameterMissingResponse();
        }

        $examination = $this->em->getRepository(Examination::class)->find($this->params->id);

        $examination->setStatus(ExaminationStatus::ACCEPTED);

        $this->em->flush();

        return $this->getResponse();
    }

    public function getPredefinedExamination(){
        $user = $this->getUser();
        $examinations = $this->em->getRepository(Examination::class)->findBy(['type' => ExaminationType::PREDEFINED]);
   
        return $this->getResponse([
            'examinations' =>  $examinations
        ]);
    }

    public function changeExaminationStatus(){
        $id = $this->request->get('examinationId');
        $version = $this->request->get('version');

        if(!is_null($version)){
            return $this->lockObject($id, $version);
        }else{
            $examination = $this->em->getRepository(Examination::class)->find($id);
            $examination->setStatus(ExaminationStatus::ACCEPTED);
            $this->em->flush();
       
            return $this->getResponse();
        }
    }

    private function lockObject($id, $version){
        try {
            $entity = $this->em->find(Examination::class, $id, LockMode::OPTIMISTIC, $version);
            $entity->setStatus(ExaminationStatus::ACCEPTED);
            if(!is_null($this->request->get('test'))){
                $user = $this->em->getRepository(User::class)->find(1);
            }else{
                $user = $this->getUser();
            }            
            $entity->setPatient($user);
            $this->em->flush();
            return $this->getResponse();
        } catch(OptimisticLockException $e) {
            return $this->getErrorResponse("Sorry, but someone else has already changed this entity. Please apply the changes again!");
        }
    }

    public function getUserProfile(){
        return $this->getResponse([
            'user' =>  $this->getUser()
        ]);
    }

    private function isAvailable($examinations, $doctor){
        $available = true;
        foreach($examinations as $ex){
            if($ex->getDoctor()->getId() == $doctor->getId()){
                $available = false;
                break;
            }
        }

        return $available;
    }
}