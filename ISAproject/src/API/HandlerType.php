<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20.2.18.
 * Time: 17.20
 */

namespace App\API;

abstract class HandlerType
{
    const User = 1;
    const SocialAuthentication = 2;
    const Mail = 3;
    const Newsletter = 4;
    const Image = 5;
    const Country = 6;
    const Language = 7;
    const CustomerPreference = 8;
    const UserGirl = 9;
    const Chat = 10;
    const Message = 11;
    const Gift = 12;
    const Report = 13;
    const MessageInbox = 14;
    const Favourite = 15;
    const UserAgency = 16;
    const AutomaticMessage = 17;
    const UserAdmin = 18;
    const Pricing = 19;
    const Terms = 20;
}