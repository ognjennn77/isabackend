<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20.2.18.
 * Time: 17.26
 */

namespace App\API\Factory;


use App\Entity\User;
use App\API\HandlerType;
use App\API\V1\UserHandler;

class ApiHandlerFactoryV1
{

    public function getHandler($type, $em, $container, $logger, $admin)
    {
        if ($type == HandlerType::User) {
            return new UserHandler($em, $container, $logger);
        }

        return null;
    }
}